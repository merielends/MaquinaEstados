﻿using System;

namespace MaquinaEstados
{
    /* Programa que imula uma máquina de estado finita */
    /* Autor: Merielen dos Santos Guidolini */
    /* Data: 13/08/2017*/
    public class Program
    {
        static void Main(string[] args)
        {
            string[] input = ReadVector();
            string state = "S1";

            foreach(string trans in input)
            {
                state = ChangeState(state, trans);
            }

            Console.WriteLine("estado " + state);
            Console.ReadLine();
        }

        static public string[] ReadVector()
        {
            try
            {
                Console.WriteLine("Digite o vetor com as transições de estado.");
                string line = Console.ReadLine();
                line = line.Replace("[", "").Replace("]", "").Replace("\"", "").Replace(" ", "");
                return line.Split(',');
            }
            catch (Exception e)
            {
                Console.WriteLine("Dados de entrada em formato incorreto.\n" + e.Message);
                return null;
            }
        }

        // Verifica todas as transições de estado
        static public string ChangeState(string state, string trans)
        {
            switch(state)
            {
                case "S1":
                    {
                        if (trans == "a")
                            return "S2";
                        else return "S1";
                    }
                case "S2":
                    {
                        switch(trans)
                        {
                            case "a":
                                return "S2";
                            case "b":
                                return "S1";
                            case "c":
                                return "S4";
                            default: return "S2";
                        }
                    }
                case "S3":
                    {
                        switch (trans)
                        {
                            case "a":
                                return "S1";
                            case "b":
                                return "S4";
                            default: return "S3";
                        }
                    }
                case "S4":
                    {
                        if (trans == "d")
                            return "S3";
                        else
                            return "S4";
                    }
                default:
                    return "S1";
            }
        }
    }
}
