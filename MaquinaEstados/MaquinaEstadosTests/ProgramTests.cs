﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MaquinaEstados.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void ChangeStateTest()
        {
            string trans = "a";
            string state = "S1";
            string stateExpect = "S2";

            string result = Program.ChangeState(state, trans);

            Assert.AreEqual(result, stateExpect);
        }
    }
}